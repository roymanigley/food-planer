import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRecipeGroup } from 'app/shared/model/recipe-group.model';

@Component({
  selector: 'jhi-recipe-group-detail',
  templateUrl: './recipe-group-detail.component.html'
})
export class RecipeGroupDetailComponent implements OnInit {
  recipeGroup: IRecipeGroup | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recipeGroup }) => (this.recipeGroup = recipeGroup));
  }

  previousState(): void {
    window.history.back();
  }
}
