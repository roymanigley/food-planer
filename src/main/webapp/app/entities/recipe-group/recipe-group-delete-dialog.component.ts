import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRecipeGroup } from 'app/shared/model/recipe-group.model';
import { RecipeGroupService } from './recipe-group.service';

@Component({
  templateUrl: './recipe-group-delete-dialog.component.html'
})
export class RecipeGroupDeleteDialogComponent {
  recipeGroup?: IRecipeGroup;

  constructor(
    protected recipeGroupService: RecipeGroupService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.recipeGroupService.delete(id).subscribe(() => {
      this.eventManager.broadcast('recipeGroupListModification');
      this.activeModal.close();
    });
  }
}
