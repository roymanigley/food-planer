import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FoodplanerSharedModule } from 'app/shared/shared.module';
import { RecipeGroupComponent } from './recipe-group.component';
import { RecipeGroupDetailComponent } from './recipe-group-detail.component';
import { RecipeGroupUpdateComponent } from './recipe-group-update.component';
import { RecipeGroupDeleteDialogComponent } from './recipe-group-delete-dialog.component';
import { recipeGroupRoute } from './recipe-group.route';

@NgModule({
  imports: [FoodplanerSharedModule, RouterModule.forChild(recipeGroupRoute)],
  declarations: [RecipeGroupComponent, RecipeGroupDetailComponent, RecipeGroupUpdateComponent, RecipeGroupDeleteDialogComponent],
  entryComponents: [RecipeGroupDeleteDialogComponent]
})
export class FoodplanerRecipeGroupModule {}
