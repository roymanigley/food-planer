import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRecipeGroup, RecipeGroup } from 'app/shared/model/recipe-group.model';
import { RecipeGroupService } from './recipe-group.service';

@Component({
  selector: 'jhi-recipe-group-update',
  templateUrl: './recipe-group-update.component.html'
})
export class RecipeGroupUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]]
  });

  constructor(protected recipeGroupService: RecipeGroupService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recipeGroup }) => {
      this.updateForm(recipeGroup);
    });
  }

  updateForm(recipeGroup: IRecipeGroup): void {
    this.editForm.patchValue({
      id: recipeGroup.id,
      name: recipeGroup.name
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const recipeGroup = this.createFromForm();
    if (recipeGroup.id !== undefined) {
      this.subscribeToSaveResponse(this.recipeGroupService.update(recipeGroup));
    } else {
      this.subscribeToSaveResponse(this.recipeGroupService.create(recipeGroup));
    }
  }

  private createFromForm(): IRecipeGroup {
    return {
      ...new RecipeGroup(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRecipeGroup>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
