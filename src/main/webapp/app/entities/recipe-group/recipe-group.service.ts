import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRecipeGroup } from 'app/shared/model/recipe-group.model';

type EntityResponseType = HttpResponse<IRecipeGroup>;
type EntityArrayResponseType = HttpResponse<IRecipeGroup[]>;

@Injectable({ providedIn: 'root' })
export class RecipeGroupService {
  public resourceUrl = SERVER_API_URL + 'api/recipe-groups';

  constructor(protected http: HttpClient) {}

  create(recipeGroup: IRecipeGroup): Observable<EntityResponseType> {
    return this.http.post<IRecipeGroup>(this.resourceUrl, recipeGroup, { observe: 'response' });
  }

  update(recipeGroup: IRecipeGroup): Observable<EntityResponseType> {
    return this.http.put<IRecipeGroup>(this.resourceUrl, recipeGroup, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRecipeGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    if (!req) {
      req = {
        page: 0,
        size: 999999,
        sort: ['name,asc', 'id']
      };
    }
    const options = createRequestOption(req);
    return this.http.get<IRecipeGroup[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
