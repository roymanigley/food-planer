import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'schedule',
        loadChildren: () => import('./schedule/schedule.module').then(m => m.FoodplanerScheduleModule)
      },
      {
        path: 'meal',
        loadChildren: () => import('./meal/meal.module').then(m => m.FoodplanerMealModule)
      },
      {
        path: 'recipe-group',
        loadChildren: () => import('./recipe-group/recipe-group.module').then(m => m.FoodplanerRecipeGroupModule)
      },
      {
        path: 'ingredient',
        loadChildren: () => import('./ingredient/ingredient.module').then(m => m.FoodplanerIngredientModule)
      },
      {
        path: 'recipe-ingredient',
        loadChildren: () => import('./recipe-ingredient/recipe-ingredient.module').then(m => m.FoodplanerRecipeIngredientModule)
      },
      {
        path: 'unit',
        loadChildren: () => import('./unit/unit.module').then(m => m.FoodplanerUnitModule)
      },
      {
        path: 'recipe',
        loadChildren: () => import('./recipe/recipe.module').then(m => m.FoodplanerRecipeModule)
      },
      {
        path: 'shopping-list',
        loadChildren: () => import('./shopping-list/shopping-list.module').then(m => m.FoodplanerShoppingListModule)
      },
      {
        path: 'shopping-list-position',
        loadChildren: () =>
          import('./shopping-list-position/shopping-list-position.module').then(m => m.FoodplanerShoppingListPositionModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class FoodplanerEntityModule {}
