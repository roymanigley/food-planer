import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IShoppingListPosition } from 'app/shared/model/shopping-list-position.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ShoppingListPositionService } from './shopping-list-position.service';
import { ShoppingListPositionDeleteDialogComponent } from './shopping-list-position-delete-dialog.component';

@Component({
  selector: 'jhi-shopping-list-position',
  templateUrl: './shopping-list-position.component.html'
})
export class ShoppingListPositionComponent implements OnInit, OnDestroy {
  shoppingListPositions: IShoppingListPosition[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected shoppingListPositionService: ShoppingListPositionService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.shoppingListPositions = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.shoppingListPositionService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IShoppingListPosition[]>) => this.paginateShoppingListPositions(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.shoppingListPositions = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInShoppingListPositions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IShoppingListPosition): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInShoppingListPositions(): void {
    this.eventSubscriber = this.eventManager.subscribe('shoppingListPositionListModification', () => this.reset());
  }

  delete(shoppingListPosition: IShoppingListPosition): void {
    const modalRef = this.modalService.open(ShoppingListPositionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.shoppingListPosition = shoppingListPosition;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateShoppingListPositions(data: IShoppingListPosition[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.shoppingListPositions.push(data[i]);
      }
    }
  }
}
