import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IShoppingListPosition, ShoppingListPosition } from 'app/shared/model/shopping-list-position.model';
import { ShoppingListPositionService } from './shopping-list-position.service';
import { ShoppingListPositionComponent } from './shopping-list-position.component';
import { ShoppingListPositionDetailComponent } from './shopping-list-position-detail.component';
import { ShoppingListPositionUpdateComponent } from './shopping-list-position-update.component';

@Injectable({ providedIn: 'root' })
export class ShoppingListPositionResolve implements Resolve<IShoppingListPosition> {
  constructor(private service: ShoppingListPositionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IShoppingListPosition> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((shoppingListPosition: HttpResponse<ShoppingListPosition>) => {
          if (shoppingListPosition.body) {
            return of(shoppingListPosition.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ShoppingListPosition());
  }
}

export const shoppingListPositionRoute: Routes = [
  {
    path: '',
    component: ShoppingListPositionComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'foodplanerApp.shoppingListPosition.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShoppingListPositionDetailComponent,
    resolve: {
      shoppingListPosition: ShoppingListPositionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'foodplanerApp.shoppingListPosition.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShoppingListPositionUpdateComponent,
    resolve: {
      shoppingListPosition: ShoppingListPositionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'foodplanerApp.shoppingListPosition.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShoppingListPositionUpdateComponent,
    resolve: {
      shoppingListPosition: ShoppingListPositionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'foodplanerApp.shoppingListPosition.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
