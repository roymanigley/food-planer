import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FoodplanerSharedModule } from 'app/shared/shared.module';
import { ShoppingListPositionComponent } from './shopping-list-position.component';
import { ShoppingListPositionDetailComponent } from './shopping-list-position-detail.component';
import { ShoppingListPositionUpdateComponent } from './shopping-list-position-update.component';
import { ShoppingListPositionDeleteDialogComponent } from './shopping-list-position-delete-dialog.component';
import { shoppingListPositionRoute } from './shopping-list-position.route';

@NgModule({
  imports: [FoodplanerSharedModule, RouterModule.forChild(shoppingListPositionRoute)],
  declarations: [
    ShoppingListPositionComponent,
    ShoppingListPositionDetailComponent,
    ShoppingListPositionUpdateComponent,
    ShoppingListPositionDeleteDialogComponent
  ],
  entryComponents: [ShoppingListPositionDeleteDialogComponent]
})
export class FoodplanerShoppingListPositionModule {}
