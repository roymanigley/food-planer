import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShoppingListPosition } from 'app/shared/model/shopping-list-position.model';
import { ShoppingListPositionService } from './shopping-list-position.service';

@Component({
  templateUrl: './shopping-list-position-delete-dialog.component.html'
})
export class ShoppingListPositionDeleteDialogComponent {
  shoppingListPosition?: IShoppingListPosition;

  constructor(
    protected shoppingListPositionService: ShoppingListPositionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.shoppingListPositionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('shoppingListPositionListModification');
      this.activeModal.close();
    });
  }
}
