import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IShoppingListPosition, ShoppingListPosition } from 'app/shared/model/shopping-list-position.model';
import { ShoppingListPositionService } from './shopping-list-position.service';
import { IIngredient } from 'app/shared/model/ingredient.model';
import { IngredientService } from 'app/entities/ingredient/ingredient.service';
import { IUnit } from 'app/shared/model/unit.model';
import { UnitService } from 'app/entities/unit/unit.service';
import { IShoppingList } from 'app/shared/model/shopping-list.model';
import { ShoppingListService } from 'app/entities/shopping-list/shopping-list.service';

type SelectableEntity = IIngredient | IUnit | IShoppingList;

@Component({
  selector: 'jhi-shopping-list-position-update',
  templateUrl: './shopping-list-position-update.component.html'
})
export class ShoppingListPositionUpdateComponent implements OnInit {
  isSaving = false;
  ingredients: IIngredient[] = [];
  units: IUnit[] = [];
  shoppinglists: IShoppingList[] = [];

  editForm = this.fb.group({
    id: [],
    quantity: [null, [Validators.required]],
    shoppingListPositionIngredient: [null, Validators.required],
    shoppingListPositionUnit: [null, Validators.required],
    shoppingList: []
  });

  constructor(
    protected shoppingListPositionService: ShoppingListPositionService,
    protected ingredientService: IngredientService,
    protected unitService: UnitService,
    protected shoppingListService: ShoppingListService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shoppingListPosition }) => {
      this.updateForm(shoppingListPosition);

      this.ingredientService.query().subscribe((res: HttpResponse<IIngredient[]>) => (this.ingredients = res.body || []));

      this.unitService.query().subscribe((res: HttpResponse<IUnit[]>) => (this.units = res.body || []));

      this.shoppingListService.query().subscribe((res: HttpResponse<IShoppingList[]>) => (this.shoppinglists = res.body || []));
    });
  }

  updateForm(shoppingListPosition: IShoppingListPosition): void {
    this.editForm.patchValue({
      id: shoppingListPosition.id,
      quantity: shoppingListPosition.quantity,
      shoppingListPositionIngredient: shoppingListPosition.shoppingListPositionIngredient,
      shoppingListPositionUnit: shoppingListPosition.shoppingListPositionUnit,
      shoppingList: shoppingListPosition.shoppingList
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shoppingListPosition = this.createFromForm();
    if (shoppingListPosition.id !== undefined) {
      this.subscribeToSaveResponse(this.shoppingListPositionService.update(shoppingListPosition));
    } else {
      this.subscribeToSaveResponse(this.shoppingListPositionService.create(shoppingListPosition));
    }
  }

  private createFromForm(): IShoppingListPosition {
    return {
      ...new ShoppingListPosition(),
      id: this.editForm.get(['id'])!.value,
      quantity: this.editForm.get(['quantity'])!.value,
      shoppingListPositionIngredient: this.editForm.get(['shoppingListPositionIngredient'])!.value,
      shoppingListPositionUnit: this.editForm.get(['shoppingListPositionUnit'])!.value,
      shoppingList: this.editForm.get(['shoppingList'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShoppingListPosition>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
