import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IShoppingListPosition } from 'app/shared/model/shopping-list-position.model';

type EntityResponseType = HttpResponse<IShoppingListPosition>;
type EntityArrayResponseType = HttpResponse<IShoppingListPosition[]>;

@Injectable({ providedIn: 'root' })
export class ShoppingListPositionService {
  public resourceUrl = SERVER_API_URL + 'api/shopping-list-positions';

  constructor(protected http: HttpClient) {}

  create(shoppingListPosition: IShoppingListPosition): Observable<EntityResponseType> {
    return this.http.post<IShoppingListPosition>(this.resourceUrl, shoppingListPosition, { observe: 'response' });
  }

  update(shoppingListPosition: IShoppingListPosition): Observable<EntityResponseType> {
    return this.http.put<IShoppingListPosition>(this.resourceUrl, shoppingListPosition, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IShoppingListPosition>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IShoppingListPosition[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
