import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShoppingListPosition } from 'app/shared/model/shopping-list-position.model';

@Component({
  selector: 'jhi-shopping-list-position-detail',
  templateUrl: './shopping-list-position-detail.component.html'
})
export class ShoppingListPositionDetailComponent implements OnInit {
  shoppingListPosition: IShoppingListPosition | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shoppingListPosition }) => (this.shoppingListPosition = shoppingListPosition));
  }

  previousState(): void {
    window.history.back();
  }
}
