import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { ISchedule, Schedule } from 'app/shared/model/schedule.model';
import { IShoppingList } from 'app/shared/model/shopping-list.model';
import { ScheduleService } from './schedule.service';

@Component({
  selector: 'jhi-schedule-update',
  templateUrl: './schedule-update.component.html'
})
export class ScheduleUpdateComponent implements OnInit {
  isSaving = false;
  startDp: any;
  endDp: any;

  editForm = this.fb.group({
    id: [],
    start: [null, [Validators.required]],
    end: [null, [Validators.required]]
  });

  constructor(
    protected scheduleService: ScheduleService,
    protected activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ schedule }) => {
      this.updateForm(schedule);
    });
  }

  updateForm(schedule: ISchedule): void {
    this.editForm.patchValue({
      id: schedule.id,
      start: schedule.start,
      end: schedule.end
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const schedule = this.createFromForm();
    if (schedule.id !== undefined) {
      this.subscribeToSaveResponse(this.scheduleService.update(schedule));
    } else {
      this.subscribeToSaveResponse(this.scheduleService.create(schedule));
    }
  }

  generateShoppingList(): void {
    this.isSaving = true;
    const schedule = this.createFromForm();
    if (schedule.id !== undefined) {
      /* eslint-disable no-console */
      console.log('****************************generateShoppingList');
      /* eslint-enable no-console */
      this.subscribeToGenerateShoppingListResponse(this.scheduleService.generateShoppingList(schedule.id));
    }
  }

  generateMeals(): void {
    this.isSaving = true;
    const schedule = this.createFromForm();
    if (schedule.id !== undefined) {
      this.subscribeToGenerateMealResponse(this.scheduleService.generateMeals(schedule.id));
    }
  }

  private createFromForm(): ISchedule {
    return {
      ...new Schedule(),
      id: this.editForm.get(['id'])!.value,
      start: this.editForm.get(['start'])!.value,
      end: this.editForm.get(['end'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISchedule>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
  protected subscribeToGenerateMealResponse(result: Observable<HttpResponse<ISchedule>>): void {
    result.subscribe(
      () => this.onGenerateMealsSuccess(result),
      () => this.onSaveError()
    );
  }
  protected subscribeToGenerateShoppingListResponse(result: Observable<HttpResponse<IShoppingList>>): void {
    result.subscribe(
      () => this.onGenerateShoppingListSuccess(result),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onGenerateMealsSuccess(result: Observable<HttpResponse<ISchedule>>): void {
    this.isSaving = false;
    result.subscribe(data => {
      if (data && data.body && data.body.id) this.router.navigate(['schedule/' + data.body.id + '/view']);
    });
  }

  protected onGenerateShoppingListSuccess(result: Observable<HttpResponse<IShoppingList>>): void {
    this.isSaving = false;

    result.subscribe(data => {
      if (data && data.body && data.body.id) this.router.navigate(['shopping-list/' + data.body.id + '/view']);
    });
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
