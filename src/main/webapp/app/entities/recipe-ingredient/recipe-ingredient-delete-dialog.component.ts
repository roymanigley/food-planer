import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRecipeIngredient } from 'app/shared/model/recipe-ingredient.model';
import { RecipeIngredientService } from './recipe-ingredient.service';

@Component({
  templateUrl: './recipe-ingredient-delete-dialog.component.html'
})
export class RecipeIngredientDeleteDialogComponent {
  recipeIngredient?: IRecipeIngredient;

  constructor(
    protected recipeIngredientService: RecipeIngredientService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.recipeIngredientService.delete(id).subscribe(() => {
      this.eventManager.broadcast('recipeIngredientListModification');
      this.activeModal.close();
    });
  }
}
