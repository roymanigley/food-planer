import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FoodplanerSharedModule } from 'app/shared/shared.module';
import { RecipeIngredientComponent } from './recipe-ingredient.component';
import { RecipeIngredientDetailComponent } from './recipe-ingredient-detail.component';
import { RecipeIngredientUpdateComponent } from './recipe-ingredient-update.component';
import { RecipeIngredientDeleteDialogComponent } from './recipe-ingredient-delete-dialog.component';
import { recipeIngredientRoute } from './recipe-ingredient.route';

@NgModule({
  imports: [FoodplanerSharedModule, RouterModule.forChild(recipeIngredientRoute)],
  declarations: [
    RecipeIngredientComponent,
    RecipeIngredientDetailComponent,
    RecipeIngredientUpdateComponent,
    RecipeIngredientDeleteDialogComponent
  ],
  entryComponents: [RecipeIngredientDeleteDialogComponent]
})
export class FoodplanerRecipeIngredientModule {}
