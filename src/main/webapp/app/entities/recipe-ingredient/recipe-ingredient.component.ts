import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IRecipeIngredient } from 'app/shared/model/recipe-ingredient.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { RecipeIngredientService } from './recipe-ingredient.service';
import { RecipeIngredientDeleteDialogComponent } from './recipe-ingredient-delete-dialog.component';

@Component({
  selector: 'jhi-recipe-ingredient',
  templateUrl: './recipe-ingredient.component.html'
})
export class RecipeIngredientComponent implements OnInit, OnDestroy {
  recipeIngredients: IRecipeIngredient[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected recipeIngredientService: RecipeIngredientService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.recipeIngredients = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.recipeIngredientService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IRecipeIngredient[]>) => this.paginateRecipeIngredients(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.recipeIngredients = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInRecipeIngredients();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IRecipeIngredient): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInRecipeIngredients(): void {
    this.eventSubscriber = this.eventManager.subscribe('recipeIngredientListModification', () => this.reset());
  }

  delete(recipeIngredient: IRecipeIngredient): void {
    const modalRef = this.modalService.open(RecipeIngredientDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.recipeIngredient = recipeIngredient;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateRecipeIngredients(data: IRecipeIngredient[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.recipeIngredients.push(data[i]);
      }
    }
  }
}
