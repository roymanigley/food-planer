import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRecipeIngredient, RecipeIngredient } from 'app/shared/model/recipe-ingredient.model';
import { RecipeIngredientService } from './recipe-ingredient.service';
import { IUnit } from 'app/shared/model/unit.model';
import { UnitService } from 'app/entities/unit/unit.service';
import { IIngredient } from 'app/shared/model/ingredient.model';
import { IngredientService } from 'app/entities/ingredient/ingredient.service';
import { IRecipe } from 'app/shared/model/recipe.model';
import { RecipeService } from 'app/entities/recipe/recipe.service';

type SelectableEntity = IUnit | IIngredient | IRecipe;

@Component({
  selector: 'jhi-recipe-ingredient-update',
  templateUrl: './recipe-ingredient-update.component.html'
})
export class RecipeIngredientUpdateComponent implements OnInit {
  isSaving = false;
  units: IUnit[] = [];
  ingredients: IIngredient[] = [];
  recipes: IRecipe[] = [];

  editForm = this.fb.group({
    id: [],
    quantity: [null, [Validators.required]],
    recipeIngredientUnit: [null, Validators.required],
    recipeIngredientIngredient: [null, Validators.required],
    recipe: []
  });

  constructor(
    protected recipeIngredientService: RecipeIngredientService,
    protected unitService: UnitService,
    protected ingredientService: IngredientService,
    protected recipeService: RecipeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recipeIngredient }) => {
      this.updateForm(recipeIngredient);

      this.unitService.query().subscribe((res: HttpResponse<IUnit[]>) => (this.units = res.body || []));

      this.ingredientService.query().subscribe((res: HttpResponse<IIngredient[]>) => (this.ingredients = res.body || []));

      this.recipeService.query().subscribe((res: HttpResponse<IRecipe[]>) => (this.recipes = res.body || []));
    });
  }

  updateForm(recipeIngredient: IRecipeIngredient): void {
    this.editForm.patchValue({
      id: recipeIngredient.id,
      quantity: recipeIngredient.quantity,
      recipeIngredientUnit: recipeIngredient.recipeIngredientUnit,
      recipeIngredientIngredient: recipeIngredient.recipeIngredientIngredient,
      recipe: recipeIngredient.recipe
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const recipeIngredient = this.createFromForm();
    if (recipeIngredient.id !== undefined) {
      this.subscribeToSaveResponse(this.recipeIngredientService.update(recipeIngredient));
    } else {
      this.subscribeToSaveResponse(this.recipeIngredientService.create(recipeIngredient));
    }
  }

  private createFromForm(): IRecipeIngredient {
    return {
      ...new RecipeIngredient(),
      id: this.editForm.get(['id'])!.value,
      quantity: this.editForm.get(['quantity'])!.value,
      recipeIngredientUnit: this.editForm.get(['recipeIngredientUnit'])!.value,
      recipeIngredientIngredient: this.editForm.get(['recipeIngredientIngredient'])!.value,
      recipe: this.editForm.get(['recipe'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRecipeIngredient>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
