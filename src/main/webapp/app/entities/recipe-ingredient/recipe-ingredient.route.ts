import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRecipeIngredient, RecipeIngredient } from 'app/shared/model/recipe-ingredient.model';
import { RecipeIngredientService } from './recipe-ingredient.service';
import { RecipeIngredientComponent } from './recipe-ingredient.component';
import { RecipeIngredientDetailComponent } from './recipe-ingredient-detail.component';
import { RecipeIngredientUpdateComponent } from './recipe-ingredient-update.component';

@Injectable({ providedIn: 'root' })
export class RecipeIngredientResolve implements Resolve<IRecipeIngredient> {
  constructor(private service: RecipeIngredientService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRecipeIngredient> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((recipeIngredient: HttpResponse<RecipeIngredient>) => {
          if (recipeIngredient.body) {
            return of(recipeIngredient.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RecipeIngredient());
  }
}

export const recipeIngredientRoute: Routes = [
  {
    path: '',
    component: RecipeIngredientComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'foodplanerApp.recipeIngredient.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RecipeIngredientDetailComponent,
    resolve: {
      recipeIngredient: RecipeIngredientResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'foodplanerApp.recipeIngredient.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RecipeIngredientUpdateComponent,
    resolve: {
      recipeIngredient: RecipeIngredientResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'foodplanerApp.recipeIngredient.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RecipeIngredientUpdateComponent,
    resolve: {
      recipeIngredient: RecipeIngredientResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'foodplanerApp.recipeIngredient.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
