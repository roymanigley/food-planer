import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IShoppingList } from 'app/shared/model/shopping-list.model';

type EntityResponseType = HttpResponse<IShoppingList>;
type EntityArrayResponseType = HttpResponse<IShoppingList[]>;

@Injectable({ providedIn: 'root' })
export class ShoppingListService {
  public resourceUrl = SERVER_API_URL + 'api/shopping-lists';

  constructor(protected http: HttpClient) {}

  create(shoppingList: IShoppingList): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shoppingList);
    return this.http
      .post<IShoppingList>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(shoppingList: IShoppingList): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shoppingList);
    return this.http
      .put<IShoppingList>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IShoppingList>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IShoppingList[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(shoppingList: IShoppingList): IShoppingList {
    const copy: IShoppingList = Object.assign({}, shoppingList, {
      date: shoppingList.date && shoppingList.date.isValid() ? shoppingList.date.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? moment(res.body.date) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((shoppingList: IShoppingList) => {
        shoppingList.date = shoppingList.date ? moment(shoppingList.date) : undefined;
      });
    }
    return res;
  }
}
