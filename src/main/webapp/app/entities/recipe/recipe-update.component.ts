import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRecipe, Recipe } from 'app/shared/model/recipe.model';
import { RecipeService } from './recipe.service';
import { IRecipeGroup } from 'app/shared/model/recipe-group.model';
import { RecipeGroupService } from 'app/entities/recipe-group/recipe-group.service';

@Component({
  selector: 'jhi-recipe-update',
  templateUrl: './recipe-update.component.html'
})
export class RecipeUpdateComponent implements OnInit {
  isSaving = false;
  recipegroups: IRecipeGroup[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    description: [],
    ratingFlavor: [null, [Validators.required, Validators.min(0), Validators.max(5)]],
    ratingHealthy: [null, [Validators.required, Validators.min(0), Validators.max(5)]],
    recipeGroup: [null, Validators.required]
  });

  constructor(
    protected recipeService: RecipeService,
    protected recipeGroupService: RecipeGroupService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ recipe }) => {
      this.updateForm(recipe);

      this.recipeGroupService.query().subscribe((res: HttpResponse<IRecipeGroup[]>) => (this.recipegroups = res.body || []));
    });
  }

  updateForm(recipe: IRecipe): void {
    this.editForm.patchValue({
      id: recipe.id,
      name: recipe.name,
      description: recipe.description,
      ratingFlavor: recipe.ratingFlavor,
      ratingHealthy: recipe.ratingHealthy,
      recipeGroup: recipe.recipeGroup
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const recipe = this.createFromForm();
    if (recipe.id !== undefined) {
      this.subscribeToSaveResponse(this.recipeService.update(recipe));
    } else {
      this.subscribeToSaveResponse(this.recipeService.create(recipe));
    }
  }

  private createFromForm(): IRecipe {
    return {
      ...new Recipe(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      ratingFlavor: this.editForm.get(['ratingFlavor'])!.value,
      ratingHealthy: this.editForm.get(['ratingHealthy'])!.value,
      recipeGroup: this.editForm.get(['recipeGroup'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRecipe>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IRecipeGroup): any {
    return item.id;
  }
}
