import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMeal } from 'app/shared/model/meal.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { MealService } from './meal.service';
import { MealDeleteDialogComponent } from './meal-delete-dialog.component';

@Component({
  selector: 'jhi-meal',
  templateUrl: './meal.component.html'
})
export class MealComponent implements OnInit, OnDestroy {
  meals: IMeal[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected mealService: MealService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.meals = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.mealService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IMeal[]>) => this.paginateMeals(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.meals = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInMeals();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IMeal): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInMeals(): void {
    this.eventSubscriber = this.eventManager.subscribe('mealListModification', () => this.reset());
  }

  delete(meal: IMeal): void {
    const modalRef = this.modalService.open(MealDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.meal = meal;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateMeals(data: IMeal[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.meals.push(data[i]);
      }
    }
  }
}
