export interface IRecipeGroup {
  id?: number;
  name?: string;
}

export class RecipeGroup implements IRecipeGroup {
  constructor(public id?: number, public name?: string) {}
}
