import { IUnit } from 'app/shared/model/unit.model';
import { IIngredient } from 'app/shared/model/ingredient.model';
import { IRecipe } from 'app/shared/model/recipe.model';

export interface IRecipeIngredient {
  id?: number;
  quantity?: number;
  recipeIngredientUnit?: IUnit;
  recipeIngredientIngredient?: IIngredient;
  recipe?: IRecipe;
}

export class RecipeIngredient implements IRecipeIngredient {
  constructor(
    public id?: number,
    public quantity?: number,
    public recipeIngredientUnit?: IUnit,
    public recipeIngredientIngredient?: IIngredient,
    public recipe?: IRecipe
  ) {}
}
