import { Moment } from 'moment';
import { IShoppingListPosition } from 'app/shared/model/shopping-list-position.model';

export interface IShoppingList {
  id?: number;
  date?: Moment;
  shoppingListPositions?: IShoppingListPosition[];
}

export class ShoppingList implements IShoppingList {
  constructor(public id?: number, public date?: Moment, public shoppingListPositions?: IShoppingListPosition[]) {}
}
