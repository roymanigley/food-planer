import { IIngredient } from 'app/shared/model/ingredient.model';
import { IUnit } from 'app/shared/model/unit.model';
import { IShoppingList } from 'app/shared/model/shopping-list.model';

export interface IShoppingListPosition {
  id?: number;
  quantity?: number;
  shoppingListPositionIngredient?: IIngredient;
  shoppingListPositionUnit?: IUnit;
  shoppingList?: IShoppingList;
}

export class ShoppingListPosition implements IShoppingListPosition {
  constructor(
    public id?: number,
    public quantity?: number,
    public shoppingListPositionIngredient?: IIngredient,
    public shoppingListPositionUnit?: IUnit,
    public shoppingList?: IShoppingList
  ) {}
}
