import { IRecipeIngredient } from 'app/shared/model/recipe-ingredient.model';
import { IRecipeGroup } from 'app/shared/model/recipe-group.model';

export interface IRecipe {
  id?: number;
  name?: string;
  description?: string;
  ratingFlavor?: number;
  ratingHealthy?: number;
  recipeIngredients?: IRecipeIngredient[];
  recipeGroup?: IRecipeGroup;
}

export class Recipe implements IRecipe {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public ratingFlavor?: number,
    public ratingHealthy?: number,
    public recipeIngredients?: IRecipeIngredient[],
    public recipeGroup?: IRecipeGroup
  ) {}
}
