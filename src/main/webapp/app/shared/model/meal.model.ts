import { Moment } from 'moment';
import { IRecipe } from 'app/shared/model/recipe.model';
import { ISchedule } from 'app/shared/model/schedule.model';

export interface IMeal {
  id?: number;
  date?: Moment;
  scheduledMealRecipe?: IRecipe;
  schedule?: ISchedule;
}

export class Meal implements IMeal {
  constructor(public id?: number, public date?: Moment, public scheduledMealRecipe?: IRecipe, public schedule?: ISchedule) {}
}
