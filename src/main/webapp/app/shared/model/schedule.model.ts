import { Moment } from 'moment';
import { IMeal } from 'app/shared/model/meal.model';

export interface ISchedule {
  id?: number;
  start?: Moment;
  end?: Moment;
  scheduledMeals?: IMeal[];
}

export class Schedule implements ISchedule {
  constructor(public id?: number, public start?: Moment, public end?: Moment, public scheduledMeals?: IMeal[]) {}
}
