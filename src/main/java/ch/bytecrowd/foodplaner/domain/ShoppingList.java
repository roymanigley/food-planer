package ch.bytecrowd.foodplaner.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A ShoppingList.
 */
@Entity
@Table(name = "shopping_list")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ShoppingList implements Serializable {

    private static final long serialVersionUID = 1L;

    public static ShoppingList of(LocalDate date) {
        final ShoppingList shoppingList = new ShoppingList();
        shoppingList.setDate(date);
        return shoppingList;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "date", nullable = false)
    private LocalDate date;

    @OneToMany(mappedBy = "shoppingList", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JsonProperty("shoppingListPositions")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ShoppingListPosition> shoppingListPositions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public ShoppingList date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Set<ShoppingListPosition> getShoppingListPositions() {
        return shoppingListPositions;
    }

    public ShoppingList shoppingListPositions(Set<ShoppingListPosition> shoppingListPositions) {
        this.shoppingListPositions = shoppingListPositions;
        return this;
    }

    public ShoppingList addShoppingListPosition(ShoppingListPosition shoppingListPosition) {
        this.shoppingListPositions.add(shoppingListPosition);
        shoppingListPosition.setShoppingList(this);
        return this;
    }

    public ShoppingList removeShoppingListPosition(ShoppingListPosition shoppingListPosition) {
        this.shoppingListPositions.remove(shoppingListPosition);
        shoppingListPosition.setShoppingList(null);
        return this;
    }

    public void setShoppingListPositions(Set<ShoppingListPosition> shoppingListPositions) {
        this.shoppingListPositions = shoppingListPositions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShoppingList)) {
            return false;
        }
        return id != null && id.equals(((ShoppingList) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ShoppingList{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
