package ch.bytecrowd.foodplaner.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ShoppingListPosition.
 */
@Entity
@Table(name = "shopping_list_position")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ShoppingListPosition implements Serializable {

    private static final long serialVersionUID = 1L;

    public static ShoppingListPosition of(ShoppingList shoppingList, Ingredient ingredient, Double quantity, Unit unit) {
        final ShoppingListPosition position = new ShoppingListPosition();
        position.setShoppingList(shoppingList);
        position.setShoppingListPositionIngredient(ingredient);
        position.setQuantity(quantity);
        position.setShoppingListPositionUnit(unit);
        return position;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "quantity", nullable = false)
    private Double quantity;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("shoppingListPositions")
    private Ingredient shoppingListPositionIngredient;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("shoppingListPositions")
    private Unit shoppingListPositionUnit;

    @ManyToOne()
    @JsonIgnoreProperties("shoppingListPositions")
    private ShoppingList shoppingList;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getQuantity() {
        return quantity;
    }

    public ShoppingListPosition quantity(Double quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Ingredient getShoppingListPositionIngredient() {
        return shoppingListPositionIngredient;
    }

    public ShoppingListPosition shoppingListPositionIngredient(Ingredient ingredient) {
        this.shoppingListPositionIngredient = ingredient;
        return this;
    }

    public void setShoppingListPositionIngredient(Ingredient ingredient) {
        this.shoppingListPositionIngredient = ingredient;
    }

    public Unit getShoppingListPositionUnit() {
        return shoppingListPositionUnit;
    }

    public ShoppingListPosition shoppingListPositionUnit(Unit unit) {
        this.shoppingListPositionUnit = unit;
        return this;
    }

    public void setShoppingListPositionUnit(Unit unit) {
        this.shoppingListPositionUnit = unit;
    }

    public ShoppingList getShoppingList() {
        return shoppingList;
    }

    public ShoppingListPosition shoppingList(ShoppingList shoppingList) {
        this.shoppingList = shoppingList;
        return this;
    }

    public void setShoppingList(ShoppingList shoppingList) {
        this.shoppingList = shoppingList;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShoppingListPosition)) {
            return false;
        }
        return id != null && id.equals(((ShoppingListPosition) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ShoppingListPosition{" +
            "id=" + getId() +
            ", quantity=" + getQuantity() +
            "}";
    }
}
