package ch.bytecrowd.foodplaner.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A RecipeIngredient.
 */
@Entity
@Table(name = "recipe_ingredient")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RecipeIngredient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "quantity", nullable = false)
    private Double quantity;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("recipeIngredients")
    private Unit recipeIngredientUnit;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("recipeIngredients")
    private Ingredient recipeIngredientIngredient;

    @ManyToOne
    @JsonIgnoreProperties("recipeIngredients")
    @JoinColumn
    private Recipe recipe;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getQuantity() {
        return quantity;
    }

    public RecipeIngredient quantity(Double quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Unit getRecipeIngredientUnit() {
        return recipeIngredientUnit;
    }

    public RecipeIngredient recipeIngredientUnit(Unit unit) {
        this.recipeIngredientUnit = unit;
        return this;
    }

    public void setRecipeIngredientUnit(Unit unit) {
        this.recipeIngredientUnit = unit;
    }

    public Ingredient getRecipeIngredientIngredient() {
        return recipeIngredientIngredient;
    }

    public RecipeIngredient recipeIngredientIngredient(Ingredient ingredient) {
        this.recipeIngredientIngredient = ingredient;
        return this;
    }

    public void setRecipeIngredientIngredient(Ingredient ingredient) {
        this.recipeIngredientIngredient = ingredient;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public RecipeIngredient recipe(Recipe recipe) {
        this.recipe = recipe;
        return this;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RecipeIngredient)) {
            return false;
        }
        return id != null && id.equals(((RecipeIngredient) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RecipeIngredient{" +
            "id=" + getId() +
            ", quantity=" + getQuantity() +
            "}";
    }
}
