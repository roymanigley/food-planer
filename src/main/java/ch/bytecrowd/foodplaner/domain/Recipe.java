package ch.bytecrowd.foodplaner.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Recipe.
 */
@Entity
@Table(name = "recipe")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Recipe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @NotNull
    @Min(value = 0)
    @Max(value = 5)
    @Column(name = "rating_flavor", nullable = false)
    private Integer ratingFlavor;

    @NotNull
    @Min(value = 0)
    @Max(value = 5)
    @Column(name = "rating_healthy", nullable = false)
    private Integer ratingHealthy;

    @OneToMany(mappedBy = "recipe", fetch = FetchType.EAGER)
    @JsonProperty("recipeIngredients")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<RecipeIngredient> recipeIngredients = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("recipes")
    private RecipeGroup recipeGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Recipe name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Recipe description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRatingFlavor() {
        return ratingFlavor;
    }

    public Recipe ratingFlavor(Integer ratingFlavor) {
        this.ratingFlavor = ratingFlavor;
        return this;
    }

    public void setRatingFlavor(Integer ratingFlavor) {
        this.ratingFlavor = ratingFlavor;
    }

    public Integer getRatingHealthy() {
        return ratingHealthy;
    }

    public Recipe ratingHealthy(Integer ratingHealthy) {
        this.ratingHealthy = ratingHealthy;
        return this;
    }

    public void setRatingHealthy(Integer ratingHealthy) {
        this.ratingHealthy = ratingHealthy;
    }

    public Set<RecipeIngredient> getRecipeIngredients() {
        return recipeIngredients;
    }

    public Recipe recipeIngredients(Set<RecipeIngredient> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
        return this;
    }

    public Recipe addRecipeIngredient(RecipeIngredient recipeIngredient) {
        this.recipeIngredients.add(recipeIngredient);
        recipeIngredient.setRecipe(this);
        return this;
    }

    public Recipe removeRecipeIngredient(RecipeIngredient recipeIngredient) {
        this.recipeIngredients.remove(recipeIngredient);
        recipeIngredient.setRecipe(null);
        return this;
    }

    public void setRecipeIngredients(Set<RecipeIngredient> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }

    public RecipeGroup getRecipeGroup() {
        return recipeGroup;
    }

    public Recipe recipeGroup(RecipeGroup recipeGroup) {
        this.recipeGroup = recipeGroup;
        return this;
    }

    public void setRecipeGroup(RecipeGroup recipeGroup) {
        this.recipeGroup = recipeGroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Recipe)) {
            return false;
        }
        return id != null && id.equals(((Recipe) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Recipe{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", ratingFlavor=" + getRatingFlavor() +
            ", ratingHealthy=" + getRatingHealthy() +
            "}";
    }
}
