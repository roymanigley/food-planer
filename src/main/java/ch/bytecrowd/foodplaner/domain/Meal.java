package ch.bytecrowd.foodplaner.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Meal.
 */
@Entity
@Table(name = "meal")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Meal implements Serializable {

    private static final long serialVersionUID = 1L;

    public static Meal of(LocalDate date, Recipe recipe, Schedule schedule) {
        final Meal meal = new Meal();
        meal.setDate(date);
        meal.setSchedule(schedule);
        schedule.addScheduledMeals(meal);
        meal.setScheduledMealRecipe(recipe);
        return meal;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "date", nullable = false)
    private LocalDate date;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("meals")
    private Recipe scheduledMealRecipe;

    @ManyToOne
    @JsonIgnoreProperties({"scheduledMeals", "shoppingListPositions"})
    private Schedule schedule;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Meal date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Recipe getScheduledMealRecipe() {
        return scheduledMealRecipe;
    }

    public Meal scheduledMealRecipe(Recipe recipe) {
        this.scheduledMealRecipe = recipe;
        return this;
    }

    public void setScheduledMealRecipe(Recipe recipe) {
        this.scheduledMealRecipe = recipe;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public Meal schedule(Schedule schedule) {
        this.schedule = schedule;
        return this;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Meal)) {
            return false;
        }
        return id != null && id.equals(((Meal) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Meal{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
