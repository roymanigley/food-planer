package ch.bytecrowd.foodplaner.repository;

import ch.bytecrowd.foodplaner.domain.RecipeGroup;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the RecipeGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecipeGroupRepository extends JpaRepository<RecipeGroup, Long> {

}
