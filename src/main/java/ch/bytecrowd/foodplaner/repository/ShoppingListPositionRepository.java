package ch.bytecrowd.foodplaner.repository;

import ch.bytecrowd.foodplaner.domain.ShoppingListPosition;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ShoppingListPosition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShoppingListPositionRepository extends JpaRepository<ShoppingListPosition, Long> {

}
