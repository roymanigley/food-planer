package ch.bytecrowd.foodplaner.repository;

import ch.bytecrowd.foodplaner.domain.Meal;

import ch.bytecrowd.foodplaner.domain.Recipe;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Meal entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MealRepository extends JpaRepository<Meal, Long> {

    @Query("SELECT m.scheduledMealRecipe FROM Meal m WHERE m.date >= current_date - 14")
    public List<Recipe> findRecipesFromTheLastTwoWeeks();

    public void deleteAllByScheduleId(Long idSchedule);
}
