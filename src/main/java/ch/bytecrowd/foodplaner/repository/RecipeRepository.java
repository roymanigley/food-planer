package ch.bytecrowd.foodplaner.repository;

import ch.bytecrowd.foodplaner.domain.Recipe;

import org.hibernate.annotations.Parameter;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Recipe entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {

    @Query("FROM Recipe r WHERE r not in (:recipes) ORDER BY r.ratingHealthy DESC, r.ratingFlavor DESC")
    public List<Recipe> findRecipesNotIn(@Param("recipes") List<Recipe> recipes, Pageable pageable);

    @Query("FROM Recipe r ORDER BY r.ratingHealthy DESC, r.ratingFlavor DESC")
    public List<Recipe> findRecipes(Pageable pageable);
}
