package ch.bytecrowd.foodplaner.web.rest;

import ch.bytecrowd.foodplaner.domain.*;
import ch.bytecrowd.foodplaner.repository.*;
import com.google.common.collect.Maps;
import io.swagger.models.auth.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MealService {

    private final MealRepository mealRepo;
    private final RecipeRepository recipeRepo;
    private final ShoppingListRepository shoppingListRepo;
    private final ShoppingListPositionRepository shoppingListPosRepo;

    @Autowired
    public MealService(MealRepository mealRepo, RecipeRepository recipeRepo, ShoppingListRepository shoppingListRepo, ShoppingListPositionRepository shoppingListPosRepo) {
        this.mealRepo = mealRepo;
        this.recipeRepo = recipeRepo;
        this.shoppingListRepo = shoppingListRepo;
        this.shoppingListPosRepo = shoppingListPosRepo;
    }

    Logger LOG = LoggerFactory.getLogger(MealService.class);
    public void generateMealsForSchedule(Schedule schedule) {

        final int daysBetween = Period.between(schedule.getStart(), schedule.getEnd()).getDays();
        LOG.error("daysBetween: " + daysBetween);
        List<Recipe> recipesFromTheLastTwoWeeks = mealRepo.findRecipesFromTheLastTwoWeeks();
        LOG.error("recipesFromTheLastTwoWeeks: " + recipesFromTheLastTwoWeeks.size());

        List<Recipe> recipesNotIn;
        if (recipesFromTheLastTwoWeeks.isEmpty())
            recipesNotIn = recipeRepo.findRecipes(PageRequest.of(0, daysBetween));
        else
            recipesNotIn = recipeRepo.findRecipesNotIn(recipesFromTheLastTwoWeeks, PageRequest.of(0, daysBetween));

        LOG.error("recipesNotIn: " + recipesNotIn.size());
        mealRepo.deleteAllByScheduleId(schedule.getId());

        class DateWrapper {
            LocalDate date = schedule.getStart().minusDays(1);
        }
        final DateWrapper dateWrapper = new DateWrapper();

        final List<Meal> scheduledMeals = recipesNotIn.stream()
            .map(recipe -> {
                dateWrapper.date = dateWrapper.date.plusDays(1);
                return Meal.of(dateWrapper.date, recipe, schedule);
            })
            .collect(Collectors.toList());
        LOG.error("dateWrapper: " + dateWrapper.date);
        LOG.error("scheduledMeals: " + scheduledMeals.size());
        mealRepo.saveAll(scheduledMeals);
    }

    public Optional<ShoppingList> generateShoppingListForSchedule(Schedule schedule) {

        Map<Ingredient, Map<Unit, Double>> shoppingListMap = new HashMap<>();

        schedule.getScheduledMeals().stream()
            .flatMap(meal -> meal.getScheduledMealRecipe().getRecipeIngredients().stream())
            .forEach(recipeIngredient -> {
                Ingredient ingredient = recipeIngredient.getRecipeIngredientIngredient();
                if (!shoppingListMap.containsKey(ingredient)) {
                    shoppingListMap.put(ingredient, new HashMap<>());
                }
                if (!shoppingListMap.get(ingredient).containsKey(recipeIngredient.getRecipeIngredientUnit())) {
                    shoppingListMap.get(ingredient).put(recipeIngredient.getRecipeIngredientUnit(), 0.);
                }
                final Double total = shoppingListMap.get(ingredient).get(recipeIngredient.getRecipeIngredientUnit());
                shoppingListMap.get(ingredient).put(recipeIngredient.getRecipeIngredientUnit(), total + recipeIngredient.getQuantity());
            });

        System.out.println("MAP: " + shoppingListMap.size());



        final ShoppingList shoppingList = shoppingListRepo.saveAndFlush(
            ShoppingList.of(LocalDate.now())
        );

        shoppingListMap.forEach((ingredient, unitQuantity) -> {
            unitQuantity.forEach((unit, quantity) -> {
                shoppingList.addShoppingListPosition(
                    ShoppingListPosition.of(shoppingList, ingredient, quantity, unit)
                );
            });
        });

        shoppingListPosRepo.saveAll(
            shoppingList.getShoppingListPositions()
        );
        return Optional.of(shoppingList);
    }
}
