/**
 * View Models used by Spring MVC REST controllers.
 */
package ch.bytecrowd.foodplaner.web.rest.vm;
