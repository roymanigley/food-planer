package ch.bytecrowd.foodplaner.web.rest;

import ch.bytecrowd.foodplaner.domain.RecipeGroup;
import ch.bytecrowd.foodplaner.repository.RecipeGroupRepository;
import ch.bytecrowd.foodplaner.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ch.bytecrowd.foodplaner.domain.RecipeGroup}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class RecipeGroupResource {

    private final Logger log = LoggerFactory.getLogger(RecipeGroupResource.class);

    private static final String ENTITY_NAME = "recipeGroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecipeGroupRepository recipeGroupRepository;

    public RecipeGroupResource(RecipeGroupRepository recipeGroupRepository) {
        this.recipeGroupRepository = recipeGroupRepository;
    }

    /**
     * {@code POST  /recipe-groups} : Create a new recipeGroup.
     *
     * @param recipeGroup the recipeGroup to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recipeGroup, or with status {@code 400 (Bad Request)} if the recipeGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/recipe-groups")
    public ResponseEntity<RecipeGroup> createRecipeGroup(@Valid @RequestBody RecipeGroup recipeGroup) throws URISyntaxException {
        log.debug("REST request to save RecipeGroup : {}", recipeGroup);
        if (recipeGroup.getId() != null) {
            throw new BadRequestAlertException("A new recipeGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecipeGroup result = recipeGroupRepository.save(recipeGroup);
        return ResponseEntity.created(new URI("/api/recipe-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /recipe-groups} : Updates an existing recipeGroup.
     *
     * @param recipeGroup the recipeGroup to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recipeGroup,
     * or with status {@code 400 (Bad Request)} if the recipeGroup is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recipeGroup couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/recipe-groups")
    public ResponseEntity<RecipeGroup> updateRecipeGroup(@Valid @RequestBody RecipeGroup recipeGroup) throws URISyntaxException {
        log.debug("REST request to update RecipeGroup : {}", recipeGroup);
        if (recipeGroup.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RecipeGroup result = recipeGroupRepository.save(recipeGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, recipeGroup.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /recipe-groups} : get all the recipeGroups.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recipeGroups in body.
     */
    @GetMapping("/recipe-groups")
    public ResponseEntity<List<RecipeGroup>> getAllRecipeGroups(Pageable pageable) {
        log.debug("REST request to get a page of RecipeGroups");
        Page<RecipeGroup> page = recipeGroupRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /recipe-groups/:id} : get the "id" recipeGroup.
     *
     * @param id the id of the recipeGroup to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recipeGroup, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/recipe-groups/{id}")
    public ResponseEntity<RecipeGroup> getRecipeGroup(@PathVariable Long id) {
        log.debug("REST request to get RecipeGroup : {}", id);
        Optional<RecipeGroup> recipeGroup = recipeGroupRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(recipeGroup);
    }

    /**
     * {@code DELETE  /recipe-groups/:id} : delete the "id" recipeGroup.
     *
     * @param id the id of the recipeGroup to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/recipe-groups/{id}")
    public ResponseEntity<Void> deleteRecipeGroup(@PathVariable Long id) {
        log.debug("REST request to delete RecipeGroup : {}", id);
        recipeGroupRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
