package ch.bytecrowd.foodplaner.web.rest;

import ch.bytecrowd.foodplaner.domain.ShoppingListPosition;
import ch.bytecrowd.foodplaner.repository.ShoppingListPositionRepository;
import ch.bytecrowd.foodplaner.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ch.bytecrowd.foodplaner.domain.ShoppingListPosition}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ShoppingListPositionResource {

    private final Logger log = LoggerFactory.getLogger(ShoppingListPositionResource.class);

    private static final String ENTITY_NAME = "shoppingListPosition";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShoppingListPositionRepository shoppingListPositionRepository;

    public ShoppingListPositionResource(ShoppingListPositionRepository shoppingListPositionRepository) {
        this.shoppingListPositionRepository = shoppingListPositionRepository;
    }

    /**
     * {@code POST  /shopping-list-positions} : Create a new shoppingListPosition.
     *
     * @param shoppingListPosition the shoppingListPosition to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shoppingListPosition, or with status {@code 400 (Bad Request)} if the shoppingListPosition has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shopping-list-positions")
    public ResponseEntity<ShoppingListPosition> createShoppingListPosition(@Valid @RequestBody ShoppingListPosition shoppingListPosition) throws URISyntaxException {
        log.debug("REST request to save ShoppingListPosition : {}", shoppingListPosition);
        if (shoppingListPosition.getId() != null) {
            throw new BadRequestAlertException("A new shoppingListPosition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShoppingListPosition result = shoppingListPositionRepository.save(shoppingListPosition);
        return ResponseEntity.created(new URI("/api/shopping-list-positions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shopping-list-positions} : Updates an existing shoppingListPosition.
     *
     * @param shoppingListPosition the shoppingListPosition to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shoppingListPosition,
     * or with status {@code 400 (Bad Request)} if the shoppingListPosition is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shoppingListPosition couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shopping-list-positions")
    public ResponseEntity<ShoppingListPosition> updateShoppingListPosition(@Valid @RequestBody ShoppingListPosition shoppingListPosition) throws URISyntaxException {
        log.debug("REST request to update ShoppingListPosition : {}", shoppingListPosition);
        if (shoppingListPosition.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShoppingListPosition result = shoppingListPositionRepository.save(shoppingListPosition);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shoppingListPosition.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shopping-list-positions} : get all the shoppingListPositions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shoppingListPositions in body.
     */
    @GetMapping("/shopping-list-positions")
    public ResponseEntity<List<ShoppingListPosition>> getAllShoppingListPositions(Pageable pageable) {
        log.debug("REST request to get a page of ShoppingListPositions");
        Page<ShoppingListPosition> page = shoppingListPositionRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shopping-list-positions/:id} : get the "id" shoppingListPosition.
     *
     * @param id the id of the shoppingListPosition to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shoppingListPosition, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shopping-list-positions/{id}")
    public ResponseEntity<ShoppingListPosition> getShoppingListPosition(@PathVariable Long id) {
        log.debug("REST request to get ShoppingListPosition : {}", id);
        Optional<ShoppingListPosition> shoppingListPosition = shoppingListPositionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(shoppingListPosition);
    }

    /**
     * {@code DELETE  /shopping-list-positions/:id} : delete the "id" shoppingListPosition.
     *
     * @param id the id of the shoppingListPosition to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shopping-list-positions/{id}")
    public ResponseEntity<Void> deleteShoppingListPosition(@PathVariable Long id) {
        log.debug("REST request to delete ShoppingListPosition : {}", id);
        shoppingListPositionRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
