package ch.bytecrowd.foodplaner.web.rest;

import ch.bytecrowd.foodplaner.FoodplanerApp;
import ch.bytecrowd.foodplaner.domain.RecipeGroup;
import ch.bytecrowd.foodplaner.repository.RecipeGroupRepository;
import ch.bytecrowd.foodplaner.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ch.bytecrowd.foodplaner.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RecipeGroupResource} REST controller.
 */
@SpringBootTest(classes = FoodplanerApp.class)
public class RecipeGroupResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private RecipeGroupRepository recipeGroupRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRecipeGroupMockMvc;

    private RecipeGroup recipeGroup;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RecipeGroupResource recipeGroupResource = new RecipeGroupResource(recipeGroupRepository);
        this.restRecipeGroupMockMvc = MockMvcBuilders.standaloneSetup(recipeGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecipeGroup createEntity(EntityManager em) {
        RecipeGroup recipeGroup = new RecipeGroup()
            .name(DEFAULT_NAME);
        return recipeGroup;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecipeGroup createUpdatedEntity(EntityManager em) {
        RecipeGroup recipeGroup = new RecipeGroup()
            .name(UPDATED_NAME);
        return recipeGroup;
    }

    @BeforeEach
    public void initTest() {
        recipeGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createRecipeGroup() throws Exception {
        int databaseSizeBeforeCreate = recipeGroupRepository.findAll().size();

        // Create the RecipeGroup
        restRecipeGroupMockMvc.perform(post("/api/recipe-groups")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(recipeGroup)))
            .andExpect(status().isCreated());

        // Validate the RecipeGroup in the database
        List<RecipeGroup> recipeGroupList = recipeGroupRepository.findAll();
        assertThat(recipeGroupList).hasSize(databaseSizeBeforeCreate + 1);
        RecipeGroup testRecipeGroup = recipeGroupList.get(recipeGroupList.size() - 1);
        assertThat(testRecipeGroup.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createRecipeGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = recipeGroupRepository.findAll().size();

        // Create the RecipeGroup with an existing ID
        recipeGroup.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecipeGroupMockMvc.perform(post("/api/recipe-groups")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(recipeGroup)))
            .andExpect(status().isBadRequest());

        // Validate the RecipeGroup in the database
        List<RecipeGroup> recipeGroupList = recipeGroupRepository.findAll();
        assertThat(recipeGroupList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = recipeGroupRepository.findAll().size();
        // set the field null
        recipeGroup.setName(null);

        // Create the RecipeGroup, which fails.

        restRecipeGroupMockMvc.perform(post("/api/recipe-groups")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(recipeGroup)))
            .andExpect(status().isBadRequest());

        List<RecipeGroup> recipeGroupList = recipeGroupRepository.findAll();
        assertThat(recipeGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRecipeGroups() throws Exception {
        // Initialize the database
        recipeGroupRepository.saveAndFlush(recipeGroup);

        // Get all the recipeGroupList
        restRecipeGroupMockMvc.perform(get("/api/recipe-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recipeGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getRecipeGroup() throws Exception {
        // Initialize the database
        recipeGroupRepository.saveAndFlush(recipeGroup);

        // Get the recipeGroup
        restRecipeGroupMockMvc.perform(get("/api/recipe-groups/{id}", recipeGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(recipeGroup.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    public void getNonExistingRecipeGroup() throws Exception {
        // Get the recipeGroup
        restRecipeGroupMockMvc.perform(get("/api/recipe-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRecipeGroup() throws Exception {
        // Initialize the database
        recipeGroupRepository.saveAndFlush(recipeGroup);

        int databaseSizeBeforeUpdate = recipeGroupRepository.findAll().size();

        // Update the recipeGroup
        RecipeGroup updatedRecipeGroup = recipeGroupRepository.findById(recipeGroup.getId()).get();
        // Disconnect from session so that the updates on updatedRecipeGroup are not directly saved in db
        em.detach(updatedRecipeGroup);
        updatedRecipeGroup
            .name(UPDATED_NAME);

        restRecipeGroupMockMvc.perform(put("/api/recipe-groups")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedRecipeGroup)))
            .andExpect(status().isOk());

        // Validate the RecipeGroup in the database
        List<RecipeGroup> recipeGroupList = recipeGroupRepository.findAll();
        assertThat(recipeGroupList).hasSize(databaseSizeBeforeUpdate);
        RecipeGroup testRecipeGroup = recipeGroupList.get(recipeGroupList.size() - 1);
        assertThat(testRecipeGroup.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingRecipeGroup() throws Exception {
        int databaseSizeBeforeUpdate = recipeGroupRepository.findAll().size();

        // Create the RecipeGroup

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecipeGroupMockMvc.perform(put("/api/recipe-groups")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(recipeGroup)))
            .andExpect(status().isBadRequest());

        // Validate the RecipeGroup in the database
        List<RecipeGroup> recipeGroupList = recipeGroupRepository.findAll();
        assertThat(recipeGroupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRecipeGroup() throws Exception {
        // Initialize the database
        recipeGroupRepository.saveAndFlush(recipeGroup);

        int databaseSizeBeforeDelete = recipeGroupRepository.findAll().size();

        // Delete the recipeGroup
        restRecipeGroupMockMvc.perform(delete("/api/recipe-groups/{id}", recipeGroup.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RecipeGroup> recipeGroupList = recipeGroupRepository.findAll();
        assertThat(recipeGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
