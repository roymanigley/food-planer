package ch.bytecrowd.foodplaner.web.rest;

import ch.bytecrowd.foodplaner.FoodplanerApp;
import ch.bytecrowd.foodplaner.domain.ShoppingListPosition;
import ch.bytecrowd.foodplaner.domain.Ingredient;
import ch.bytecrowd.foodplaner.domain.Unit;
import ch.bytecrowd.foodplaner.repository.ShoppingListPositionRepository;
import ch.bytecrowd.foodplaner.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ch.bytecrowd.foodplaner.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ShoppingListPositionResource} REST controller.
 */
@SpringBootTest(classes = FoodplanerApp.class)
public class ShoppingListPositionResourceIT {

    private static final Double DEFAULT_QUANTITY = 1D;
    private static final Double UPDATED_QUANTITY = 2D;

    @Autowired
    private ShoppingListPositionRepository shoppingListPositionRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShoppingListPositionMockMvc;

    private ShoppingListPosition shoppingListPosition;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShoppingListPositionResource shoppingListPositionResource = new ShoppingListPositionResource(shoppingListPositionRepository);
        this.restShoppingListPositionMockMvc = MockMvcBuilders.standaloneSetup(shoppingListPositionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingListPosition createEntity(EntityManager em) {
        ShoppingListPosition shoppingListPosition = new ShoppingListPosition()
            .quantity(DEFAULT_QUANTITY);
        // Add required entity
        Ingredient ingredient;
        if (TestUtil.findAll(em, Ingredient.class).isEmpty()) {
            ingredient = IngredientResourceIT.createEntity(em);
            em.persist(ingredient);
            em.flush();
        } else {
            ingredient = TestUtil.findAll(em, Ingredient.class).get(0);
        }
        shoppingListPosition.setShoppingListPositionIngredient(ingredient);
        // Add required entity
        Unit unit;
        if (TestUtil.findAll(em, Unit.class).isEmpty()) {
            unit = UnitResourceIT.createEntity(em);
            em.persist(unit);
            em.flush();
        } else {
            unit = TestUtil.findAll(em, Unit.class).get(0);
        }
        shoppingListPosition.setShoppingListPositionUnit(unit);
        return shoppingListPosition;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingListPosition createUpdatedEntity(EntityManager em) {
        ShoppingListPosition shoppingListPosition = new ShoppingListPosition()
            .quantity(UPDATED_QUANTITY);
        // Add required entity
        Ingredient ingredient;
        if (TestUtil.findAll(em, Ingredient.class).isEmpty()) {
            ingredient = IngredientResourceIT.createUpdatedEntity(em);
            em.persist(ingredient);
            em.flush();
        } else {
            ingredient = TestUtil.findAll(em, Ingredient.class).get(0);
        }
        shoppingListPosition.setShoppingListPositionIngredient(ingredient);
        // Add required entity
        Unit unit;
        if (TestUtil.findAll(em, Unit.class).isEmpty()) {
            unit = UnitResourceIT.createUpdatedEntity(em);
            em.persist(unit);
            em.flush();
        } else {
            unit = TestUtil.findAll(em, Unit.class).get(0);
        }
        shoppingListPosition.setShoppingListPositionUnit(unit);
        return shoppingListPosition;
    }

    @BeforeEach
    public void initTest() {
        shoppingListPosition = createEntity(em);
    }

    @Test
    @Transactional
    public void createShoppingListPosition() throws Exception {
        int databaseSizeBeforeCreate = shoppingListPositionRepository.findAll().size();

        // Create the ShoppingListPosition
        restShoppingListPositionMockMvc.perform(post("/api/shopping-list-positions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingListPosition)))
            .andExpect(status().isCreated());

        // Validate the ShoppingListPosition in the database
        List<ShoppingListPosition> shoppingListPositionList = shoppingListPositionRepository.findAll();
        assertThat(shoppingListPositionList).hasSize(databaseSizeBeforeCreate + 1);
        ShoppingListPosition testShoppingListPosition = shoppingListPositionList.get(shoppingListPositionList.size() - 1);
        assertThat(testShoppingListPosition.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
    }

    @Test
    @Transactional
    public void createShoppingListPositionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shoppingListPositionRepository.findAll().size();

        // Create the ShoppingListPosition with an existing ID
        shoppingListPosition.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShoppingListPositionMockMvc.perform(post("/api/shopping-list-positions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingListPosition)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingListPosition in the database
        List<ShoppingListPosition> shoppingListPositionList = shoppingListPositionRepository.findAll();
        assertThat(shoppingListPositionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkQuantityIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingListPositionRepository.findAll().size();
        // set the field null
        shoppingListPosition.setQuantity(null);

        // Create the ShoppingListPosition, which fails.

        restShoppingListPositionMockMvc.perform(post("/api/shopping-list-positions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingListPosition)))
            .andExpect(status().isBadRequest());

        List<ShoppingListPosition> shoppingListPositionList = shoppingListPositionRepository.findAll();
        assertThat(shoppingListPositionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllShoppingListPositions() throws Exception {
        // Initialize the database
        shoppingListPositionRepository.saveAndFlush(shoppingListPosition);

        // Get all the shoppingListPositionList
        restShoppingListPositionMockMvc.perform(get("/api/shopping-list-positions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingListPosition.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getShoppingListPosition() throws Exception {
        // Initialize the database
        shoppingListPositionRepository.saveAndFlush(shoppingListPosition);

        // Get the shoppingListPosition
        restShoppingListPositionMockMvc.perform(get("/api/shopping-list-positions/{id}", shoppingListPosition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(shoppingListPosition.getId().intValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingShoppingListPosition() throws Exception {
        // Get the shoppingListPosition
        restShoppingListPositionMockMvc.perform(get("/api/shopping-list-positions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShoppingListPosition() throws Exception {
        // Initialize the database
        shoppingListPositionRepository.saveAndFlush(shoppingListPosition);

        int databaseSizeBeforeUpdate = shoppingListPositionRepository.findAll().size();

        // Update the shoppingListPosition
        ShoppingListPosition updatedShoppingListPosition = shoppingListPositionRepository.findById(shoppingListPosition.getId()).get();
        // Disconnect from session so that the updates on updatedShoppingListPosition are not directly saved in db
        em.detach(updatedShoppingListPosition);
        updatedShoppingListPosition
            .quantity(UPDATED_QUANTITY);

        restShoppingListPositionMockMvc.perform(put("/api/shopping-list-positions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedShoppingListPosition)))
            .andExpect(status().isOk());

        // Validate the ShoppingListPosition in the database
        List<ShoppingListPosition> shoppingListPositionList = shoppingListPositionRepository.findAll();
        assertThat(shoppingListPositionList).hasSize(databaseSizeBeforeUpdate);
        ShoppingListPosition testShoppingListPosition = shoppingListPositionList.get(shoppingListPositionList.size() - 1);
        assertThat(testShoppingListPosition.getQuantity()).isEqualTo(UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void updateNonExistingShoppingListPosition() throws Exception {
        int databaseSizeBeforeUpdate = shoppingListPositionRepository.findAll().size();

        // Create the ShoppingListPosition

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShoppingListPositionMockMvc.perform(put("/api/shopping-list-positions")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shoppingListPosition)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingListPosition in the database
        List<ShoppingListPosition> shoppingListPositionList = shoppingListPositionRepository.findAll();
        assertThat(shoppingListPositionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShoppingListPosition() throws Exception {
        // Initialize the database
        shoppingListPositionRepository.saveAndFlush(shoppingListPosition);

        int databaseSizeBeforeDelete = shoppingListPositionRepository.findAll().size();

        // Delete the shoppingListPosition
        restShoppingListPositionMockMvc.perform(delete("/api/shopping-list-positions/{id}", shoppingListPosition.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShoppingListPosition> shoppingListPositionList = shoppingListPositionRepository.findAll();
        assertThat(shoppingListPositionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
