package ch.bytecrowd.foodplaner.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ch.bytecrowd.foodplaner.web.rest.TestUtil;

public class ShoppingListPositionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShoppingListPosition.class);
        ShoppingListPosition shoppingListPosition1 = new ShoppingListPosition();
        shoppingListPosition1.setId(1L);
        ShoppingListPosition shoppingListPosition2 = new ShoppingListPosition();
        shoppingListPosition2.setId(shoppingListPosition1.getId());
        assertThat(shoppingListPosition1).isEqualTo(shoppingListPosition2);
        shoppingListPosition2.setId(2L);
        assertThat(shoppingListPosition1).isNotEqualTo(shoppingListPosition2);
        shoppingListPosition1.setId(null);
        assertThat(shoppingListPosition1).isNotEqualTo(shoppingListPosition2);
    }
}
