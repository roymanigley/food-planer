package ch.bytecrowd.foodplaner.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ch.bytecrowd.foodplaner.web.rest.TestUtil;

public class RecipeGroupTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecipeGroup.class);
        RecipeGroup recipeGroup1 = new RecipeGroup();
        recipeGroup1.setId(1L);
        RecipeGroup recipeGroup2 = new RecipeGroup();
        recipeGroup2.setId(recipeGroup1.getId());
        assertThat(recipeGroup1).isEqualTo(recipeGroup2);
        recipeGroup2.setId(2L);
        assertThat(recipeGroup1).isNotEqualTo(recipeGroup2);
        recipeGroup1.setId(null);
        assertThat(recipeGroup1).isNotEqualTo(recipeGroup2);
    }
}
