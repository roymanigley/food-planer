package ch.bytecrowd.foodplaner.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ch.bytecrowd.foodplaner.web.rest.TestUtil;

public class RecipeIngredientTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecipeIngredient.class);
        RecipeIngredient recipeIngredient1 = new RecipeIngredient();
        recipeIngredient1.setId(1L);
        RecipeIngredient recipeIngredient2 = new RecipeIngredient();
        recipeIngredient2.setId(recipeIngredient1.getId());
        assertThat(recipeIngredient1).isEqualTo(recipeIngredient2);
        recipeIngredient2.setId(2L);
        assertThat(recipeIngredient1).isNotEqualTo(recipeIngredient2);
        recipeIngredient1.setId(null);
        assertThat(recipeIngredient1).isNotEqualTo(recipeIngredient2);
    }
}
