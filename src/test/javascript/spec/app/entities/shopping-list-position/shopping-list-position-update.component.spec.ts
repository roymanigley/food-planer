import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FoodplanerTestModule } from '../../../test.module';
import { ShoppingListPositionUpdateComponent } from 'app/entities/shopping-list-position/shopping-list-position-update.component';
import { ShoppingListPositionService } from 'app/entities/shopping-list-position/shopping-list-position.service';
import { ShoppingListPosition } from 'app/shared/model/shopping-list-position.model';

describe('Component Tests', () => {
  describe('ShoppingListPosition Management Update Component', () => {
    let comp: ShoppingListPositionUpdateComponent;
    let fixture: ComponentFixture<ShoppingListPositionUpdateComponent>;
    let service: ShoppingListPositionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FoodplanerTestModule],
        declarations: [ShoppingListPositionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ShoppingListPositionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShoppingListPositionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShoppingListPositionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShoppingListPosition(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShoppingListPosition();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
