import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { FoodplanerTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { ShoppingListPositionDeleteDialogComponent } from 'app/entities/shopping-list-position/shopping-list-position-delete-dialog.component';
import { ShoppingListPositionService } from 'app/entities/shopping-list-position/shopping-list-position.service';

describe('Component Tests', () => {
  describe('ShoppingListPosition Management Delete Component', () => {
    let comp: ShoppingListPositionDeleteDialogComponent;
    let fixture: ComponentFixture<ShoppingListPositionDeleteDialogComponent>;
    let service: ShoppingListPositionService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FoodplanerTestModule],
        declarations: [ShoppingListPositionDeleteDialogComponent]
      })
        .overrideTemplate(ShoppingListPositionDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShoppingListPositionDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShoppingListPositionService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
