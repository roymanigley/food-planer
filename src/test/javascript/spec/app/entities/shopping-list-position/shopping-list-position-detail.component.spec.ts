import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FoodplanerTestModule } from '../../../test.module';
import { ShoppingListPositionDetailComponent } from 'app/entities/shopping-list-position/shopping-list-position-detail.component';
import { ShoppingListPosition } from 'app/shared/model/shopping-list-position.model';

describe('Component Tests', () => {
  describe('ShoppingListPosition Management Detail Component', () => {
    let comp: ShoppingListPositionDetailComponent;
    let fixture: ComponentFixture<ShoppingListPositionDetailComponent>;
    const route = ({ data: of({ shoppingListPosition: new ShoppingListPosition(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FoodplanerTestModule],
        declarations: [ShoppingListPositionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ShoppingListPositionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShoppingListPositionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load shoppingListPosition on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shoppingListPosition).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
