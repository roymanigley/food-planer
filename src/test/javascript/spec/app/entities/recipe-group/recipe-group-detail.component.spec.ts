import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FoodplanerTestModule } from '../../../test.module';
import { RecipeGroupDetailComponent } from 'app/entities/recipe-group/recipe-group-detail.component';
import { RecipeGroup } from 'app/shared/model/recipe-group.model';

describe('Component Tests', () => {
  describe('RecipeGroup Management Detail Component', () => {
    let comp: RecipeGroupDetailComponent;
    let fixture: ComponentFixture<RecipeGroupDetailComponent>;
    const route = ({ data: of({ recipeGroup: new RecipeGroup(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FoodplanerTestModule],
        declarations: [RecipeGroupDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RecipeGroupDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RecipeGroupDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load recipeGroup on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.recipeGroup).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
