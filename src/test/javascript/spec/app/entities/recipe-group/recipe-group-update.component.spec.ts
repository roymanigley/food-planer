import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FoodplanerTestModule } from '../../../test.module';
import { RecipeGroupUpdateComponent } from 'app/entities/recipe-group/recipe-group-update.component';
import { RecipeGroupService } from 'app/entities/recipe-group/recipe-group.service';
import { RecipeGroup } from 'app/shared/model/recipe-group.model';

describe('Component Tests', () => {
  describe('RecipeGroup Management Update Component', () => {
    let comp: RecipeGroupUpdateComponent;
    let fixture: ComponentFixture<RecipeGroupUpdateComponent>;
    let service: RecipeGroupService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FoodplanerTestModule],
        declarations: [RecipeGroupUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(RecipeGroupUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RecipeGroupUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RecipeGroupService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new RecipeGroup(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new RecipeGroup();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
