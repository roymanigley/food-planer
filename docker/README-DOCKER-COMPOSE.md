# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.
  
Build command:  
`./mvnw -ntp -Pprod verify jib:dockerBuild`

## Configured Docker services

### Applications and dependencies:

- foodplaner (monolith application)
- foodplaner's postgresql database

### Additional Services:
